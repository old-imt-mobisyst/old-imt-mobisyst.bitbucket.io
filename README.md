# WebPages

Mardom webpages based on [marked](https://marked.js.org).
The simple solution serve a website feed with Markdown files, and permit to generate content from ressources on other git repositories.

## Structure:

The solution is based on :

- **index.md**   : the served data by default.
- **xyz.md**     : pages accecible on resquest (exemple: `http://my.url.extend?README`).
- **webpages.json** : 

## Dev environment:

Install node.js server: 

```sh
sudo apt install npm
npm install http-server
```

serve...

```sh
npx http-server
```

and connect the server with your favorith webbrother.


## Solution :

- `bin/upgrade` : 

## generated files :

- **index.html** : The entrance point of your site. Load the appropriate JS libs and the md file to serve.